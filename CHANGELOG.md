- 1812.1
    - Se agregan las clases del requerimiento principal 
    del web service funcionando correctamente

- 1812.2
    - Se agrega soporte dual para conexion a base de datos local y remota
    
- 1812.3
    - Se agrega servicio para comprobar nueva version, falta agregar descarga de app

- 1812.4-Test
    - Se agrega opcion de descarga de app (se debe subir una nueva actualizacion cada vez que se agrega la app nueva)
    
- 1812.5-RC
    - Se elimina servicio web de descarga ya que se ha implementado
    externamente
   
- 1812.6-RC
    - Se agrega funcionalidad para verificar si existen regalos nuevos

- 1812.7-RC
    - Se agrega funcionalidad para actualizar version de app directamente
    en web service
    - Se optimiza servicio setRead utilizando procedimientos
    almacenados
    
- 1812.7.1-RC
    - Se agrega un servicio islast para solo pruebas

- 1905.1-RC
    - Migracion a servicios Hibernate

- 1905.2-RC
    - Correcciones menores

- 1905.2.1-RC
    - Configuracion para AWS

- 1905.2.2-RC
    - Se cambia configuracion para conectarse a aws definitivamente

- 1905.2.3
    - Correccion de error en setRead

- 1908.1
	- Se cambian parametros de conexion
