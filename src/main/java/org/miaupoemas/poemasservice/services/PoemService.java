package org.miaupoemas.poemasservice.services;

import org.miaupoemas.model.Poem;
import org.miaupoemas.poemasservice.repository.PoemRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class PoemService {
    private PoemRepository repository;

    @Autowired
    public PoemService(PoemRepository repository) {
        this.repository = repository;
    }

    public void add(Poem poem) {
        this.repository.save(poem);
    }

    public Poem getByPresent(int presentId) {
        return this.repository.getPoemByPresentId(presentId);
    }

    public List<Poem> getAll() {
        return this.repository.findAll();
    }

}
