package org.miaupoemas.poemasservice.services;

import org.miaupoemas.model.ViewPresentMenu;
import org.miaupoemas.poemasservice.repository.ViewPresentMenuRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class ViewPresentMenuService {
    private ViewPresentMenuRepository repository;

    @Autowired
    public ViewPresentMenuService(ViewPresentMenuRepository repository) {
        this.repository = repository;
    }

    public long countViews() {
        return this.repository.count();
    }

    public List<ViewPresentMenu> getAll() {
        return this.repository.findAll();
    }

}
