package org.miaupoemas.poemasservice.services;

import org.miaupoemas.model.Present;
import org.miaupoemas.poemasservice.repository.PresentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

public class PresentService {
    private PresentRepository repository;

    @Autowired
    public PresentService(PresentRepository repository) {
        this.repository = repository;
    }

    public boolean hasNew() {
        return getNew() != null;
    }

    public void add(Present present) {
        this.repository.save(present);
    }

    public List<Present> getAll() {
        return this.repository.findAll();
    }

    public Present getNew() {
        return this.repository.getFirstByReadedIsFalse();
    }

    public Present get(int id) {
        return this.repository.getPresentById(id);
    }

    public void setRead(int id) {
        Present present = get(id);
        if (present != null) {
            present.setReaded(true);
            this.repository.save(present);
        }
    }

}
