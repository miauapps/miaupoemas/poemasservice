package org.miaupoemas.poemasservice.services;

import org.miaupoemas.model.App;
import org.miaupoemas.poemasservice.repository.AppRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class AppService {
    private AppRepository repository;

    @Autowired
    public AppService(AppRepository repository) {
        this.repository = repository;
    }

    private List<App> getAllApps() {
        return this.repository.findAll();
    }

    public boolean isLast(String currentVersion) {
        App last = getLast();
        return last == null || currentVersion.equals(last.getVersion());
    }

    public App getLast() {
        List<App> listApps = getAllApps();
        return listApps.get(listApps.size()-1);
    }

    public List<String> getAllVersions() {
        List<String> listVersions = new ArrayList<>();
        List<App> apps = this.repository.findAll();
        apps.forEach(app-> listVersions.add(app.getVersion()));
        return listVersions;
    }

    public void updateApp(String newVersion) {
        App last = getLast();
        if (last != null) {
            last.setVersion(newVersion);
            this.repository.save(last);
        }
    }


}
