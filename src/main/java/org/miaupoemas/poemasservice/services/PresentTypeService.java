package org.miaupoemas.poemasservice.services;

import org.miaupoemas.model.PresentType;
import org.miaupoemas.poemasservice.repository.PresentTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

public class PresentTypeService {
    private PresentTypeRepository repository;

    @Autowired
    public PresentTypeService(PresentTypeRepository repository) {
        this.repository = repository;
    }

    public PresentType get(int id) {
        return this.repository.getPresentTypeById(id);
    }

    public List<PresentType> getAll() {
        return this.repository.findAll();
    }

}
