package org.miaupoemas.poemasservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PoemasServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(PoemasServiceApplication.class, args);
    }
}
