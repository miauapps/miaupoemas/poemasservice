package org.miaupoemas.poemasservice.repository;

import org.miaupoemas.model.PresentType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PresentTypeRepository extends JpaRepository<PresentType, Integer> {
    public PresentType getPresentTypeById(Integer id);
}
