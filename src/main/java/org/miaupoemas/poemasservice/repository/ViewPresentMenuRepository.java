package org.miaupoemas.poemasservice.repository;

import org.miaupoemas.model.ViewPresentMenu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ViewPresentMenuRepository extends JpaRepository<ViewPresentMenu, Integer> {
}
