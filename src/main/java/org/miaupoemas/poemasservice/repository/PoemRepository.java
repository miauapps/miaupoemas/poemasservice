package org.miaupoemas.poemasservice.repository;

import org.miaupoemas.model.Poem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PoemRepository extends JpaRepository<Poem, Integer> {
    public Poem getPoemByPresentId(Integer presentId);
}
