package org.miaupoemas.poemasservice.repository;

import org.miaupoemas.model.App;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppRepository extends JpaRepository<App, Integer> {
}


