package org.miaupoemas.poemasservice.repository;

import org.miaupoemas.model.Present;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PresentRepository extends JpaRepository<Present, Integer> {
    public Present getFirstByReadedIsFalse();
    public Present getPresentById(Integer id);
}
