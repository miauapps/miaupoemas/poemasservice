package org.miaupoemas.poemasservice.controller;

import org.miaupoemas.model.Present;
import org.miaupoemas.poemasservice.services.PresentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/present")
public class PresentController {

    @Autowired
    private PresentService service;

    @RequestMapping(path = "", method = RequestMethod.POST)
    public void add(@Valid @RequestBody Present present) {
        service.add(present);
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public List<Present> getAll() {
        return service.getAll();
    }

    @RequestMapping(path = "/hasnew", method = RequestMethod.GET)
    public boolean hasNewPresent() {
        return service.hasNew();
    }

    @RequestMapping(path = "/{id}")
    public Present get(@PathVariable int id) {
        return service.get(id);
    }

    @RequestMapping(path = "/read/{id}", method = RequestMethod.PATCH)
    public void setRead(@Valid @PathVariable int id) {
        service.setRead(id);
    }

}
