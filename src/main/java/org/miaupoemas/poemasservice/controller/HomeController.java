package org.miaupoemas.poemasservice.controller;

import org.miaupoemas.model.App;
import org.miaupoemas.poemasservice.services.AppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    private AppService appService;

    @Autowired
    public HomeController(AppService appService) {
        this.appService = appService;
    }

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String home() {
        App last = appService.getLast();
        if (last == null)
            return "Welcome to Miau Service";
        else
            return "Welcome to Miau Service\nApp Version: "+last.getVersion();
    }
}
