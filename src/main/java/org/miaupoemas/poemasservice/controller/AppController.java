package org.miaupoemas.poemasservice.controller;

import org.miaupoemas.model.App;
import org.miaupoemas.poemasservice.services.AppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/version")
public class AppController {

    @Autowired
    private AppService service;

    @RequestMapping(path = "/islast/{current}", method = RequestMethod.GET)
    public boolean islast(@PathVariable String current) throws SQLException {
        return service.isLast(current);
    }

    @RequestMapping(path = "/getall", method = RequestMethod.GET)
    public List<String> getAll() {
        return service.getAllVersions();
    }

    @RequestMapping(path = "/getlast", method = RequestMethod.GET)
    public String getLast() {
        App last = service.getLast();
        return last == null ? null : last.getVersion();
    }

    @RequestMapping(path = "/update", method = RequestMethod.PATCH)
    public void updateApp(@Valid @RequestBody String newVersion) {
        service.updateApp(newVersion);
    }

}
