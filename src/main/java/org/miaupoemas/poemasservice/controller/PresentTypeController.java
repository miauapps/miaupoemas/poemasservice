package org.miaupoemas.poemasservice.controller;

import org.miaupoemas.model.PresentType;
import org.miaupoemas.poemasservice.services.PresentService;
import org.miaupoemas.poemasservice.services.PresentTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/type")
public class PresentTypeController {

    @Autowired
    private PresentTypeService service;

    //@Autowired -> spring crea instancia automatica de una variable
    // es decir se autoinicializa
    @RequestMapping(path = "", method = RequestMethod.GET)
    public List<PresentType> getAll() {
        return service.getAll();
    }

}
