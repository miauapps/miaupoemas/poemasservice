package org.miaupoemas.poemasservice.controller;

import org.miaupoemas.model.Poem;
import org.miaupoemas.poemasservice.services.PoemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/poem")
public class PoemController {

    @Autowired
    private PoemService service;

    @RequestMapping(path = "", method = RequestMethod.POST)
    public void add(@Valid @RequestBody Poem poem) {
        service.add(poem);
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public List<Poem> getAll() {
        return service.getAll();
    }

    @RequestMapping(path = "/{idPresent}", method = RequestMethod.GET)
    public Poem getByPresent(@PathVariable int idPresent) {
        return service.getByPresent(idPresent);
    }

}
