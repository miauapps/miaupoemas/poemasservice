package org.miaupoemas.poemasservice.controller;

import org.miaupoemas.model.ViewPresentMenu;
import org.miaupoemas.poemasservice.services.ViewPresentMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/viewmenu")
public class ViewPresentMenuController {

    @Autowired
    private ViewPresentMenuService service;

    @RequestMapping(path = "/count", method = RequestMethod.GET)
    public long getViewCount() {
        return service.countViews();
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public List<ViewPresentMenu> getAllViews() {
        return service.getAll();
    }

}
