package org.miaupoemas.poemasservice;

import org.miaupoemas.model.Poem;
import org.miaupoemas.model.ViewPresentMenu;
import org.miaupoemas.poemasservice.repository.*;
import org.miaupoemas.poemasservice.services.*;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
@EntityScan(basePackageClasses = {Poem.class, ViewPresentMenu.class})
@EnableJpaRepositories(basePackageClasses = {PoemRepository.class, ViewPresentMenuRepository.class})
@EnableJpaAuditing
public class ApplicationConfiguration extends WebMvcConfigurationSupport {

    @Bean
    protected AppService getAppService(AppRepository repository) {
        return new AppService(repository);
    }

    @Bean
    protected PoemService getPoemService(PoemRepository repository) {
        return new PoemService(repository);
    }

    @Bean
    protected PresentService getPresentService(PresentRepository repository) {
        return new PresentService(repository);
    }

    @Bean
    protected PresentTypeService getPresentTypeService(PresentTypeRepository repository) {
        return new PresentTypeService(repository);
    }
    @Bean
    protected ViewPresentMenuService getViewPresentMenuService(ViewPresentMenuRepository repository) {
        return new ViewPresentMenuService(repository);
    }

}
