drop database if exists dbPoemas;
create database dbPoemas;
use dbPoemas;

create table icon(
    id int not null auto_increment primary key,
    data blob
);

create table presentType(
   id int not null auto_increment primary key,
   name varchar(40)
);

create table present(
  id int not null auto_increment primary key,
  idType int,
  icon blob,
  datetime datetime,
  foreign key(idType) references presentType(id)
);

create table poem(
  id int not null auto_increment primary key,
  present int,
  title varchar(40),
  body text,
  foreign key(present) references present(id)
);

create table app(
  id int not null auto_increment primary key,
  version varchar(10),
  file blob
);

create table user(
  id int not null auto_increment primary key,
  icon int not null,
  nick varchar(20),
  passw blob,
  foreign key(icon) references icon(id)
);

delimiter //
create function getKey() returns varchar(10)
READS SQL DATA NOT DETERMINISTIC
begin
  return 'powerx1111';
end//
delimiter ;

alter table present add readed bit not null;
update present set readed = 1;



alter table present add origin int not null
foreign key(origin) references user(id);

alter table present add destiny int not null
foreign key(destiny) references user(id)

desc present;


-- alter table app add file blob;
-- insert into app values(null, '1812.2', null);


drop view viewPresentMenu;
-- Vista para el menu
create view viewPresentMenu as
select p.id, p.icon, po.title, p.datetime, p.readed
from present p, poem po
where p.id = po.present;


delimiter //
create procedure updateApp(version varchar(10))
begin
    update app set app.version = version;

end//
delimiter ;


delimiter //
create procedure readPresent(id int)
begin
  update present set readed = 1 where present.id = id;

end//
delimiter ;


delimiter //
create procedure unreadPresent(id int)
begin
  update present set readed = 0 where present.id = id;

end//
delimiter ;


delimiter //
create procedure addPresent(
  type int, icon blob,
  title varchar(40), body text)
begin
  insert into present values(null, type, icon, (select now()), 0);
  insert into poem values(null, (select LAST_INSERT_ID()), title, body);
end//
delimiter ;
